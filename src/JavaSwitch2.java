import java.util.stream.StreamSupport;

public class JavaSwitch2 {
    public static void main(String[] args) {
        int day = 4;
        switch (day) {
            case 6:
                System.out.println("Today is saturday");
                break;
            case 7:
                System.out.println("Today is Sunday");
                break;
            default:
                System.out.println("Looking forword to the Weekend");
        }
        // Outputs "Looking forword to the Weekend"
    }
}
